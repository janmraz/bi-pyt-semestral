# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.
import dash
import dash_table
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
import pandas as pd
import random
from sys import argv
import sys

from functions import mps_attendance, party_attendance, worst_attendance_mps, rebel_mps_prepare_table, rebel_mps, \
    unity_party, correlation_n, correlation_a, correlation_mp_n, correlation_mp_a, full_percentual_attendance_party, \
    number_of_mps_per_party_in_time

if len(sys.argv) < 2:
    sys.exit(
        "Wrong number of input parameters - " + str(len(sys.argv)) + ", it should be python3 scraper.csv <source_file>")

arg1, arg2 = argv

pd.options.plotting.backend = "plotly"

# create dash app
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

# load the data from saved
df = pd.read_csv(arg2)

# min vote id
min_vote_id = df["Vote Id"].min()

# TODO fetch real-time data
# load data, get maximum id and then try to scrape next ones until you fail, save data afterwards

# update data according to our needs
df.MP = df.MP + " (" + df.Party + ")"
df["Vote Id"] = df["Vote Id"] - min_vote_id + 1

# create partial data frames
df_a = df[df.Vote == "A"]
df_n = df[df.Vote == "N"]
df_z = df[df.Vote == "Z"]
df_0 = df[df.Vote == "0"]
df_m = df[df.Vote == "M"]

navbar = dbc.NavbarSimple(
    children=[
        dbc.NavItem(dbc.NavLink("Docházka", href="#dochazka")),
        dbc.NavItem(dbc.NavLink("Hlasování", href="#hlasovani")),
        dbc.NavItem(dbc.NavLink("Hlasovácí koalice", href="#hlasovaci-koalice")),
        dbc.NavItem(dbc.NavLink("Ostatní", href="#ostatni")),
    ],
    brand="Vizualizace dat z hlasování v poslanecké sněmovně",
    brand_href="#output",
    color="primary",
    dark=True,
    fixed="top"
)

app.layout = html.Div([
    navbar,
    dbc.Container(children=[
        html.Div(id='output'),
        html.Br(),
        html.H2(children='Docházka', id="dochazka"),
        html.Br(),
        html.H4(children='Procentuální účast na hlasování'),
        dcc.Graph(
            id='party-attendance-percentual',
            figure=full_percentual_attendance_party(df)
        ),
        html.H4(children='Vývoj docházky jednotlivých poslanců na hlasování'),
        dcc.Dropdown(
            id='mps-dropdown',
            options=[{"label": item, "value": item} for item in df.MP.unique()],
            value=random.choices(df.MP.unique(), k=2),
            multi=True
        ),
        dcc.Graph(
            id='mps-attendance-graph',
            figure=mps_attendance([], df)
        ),
        html.H4(children='Vývoj docházky jednotlivých stran'),
        dcc.Dropdown(
            id='party-dropdown',
            options=[{"label": item, "value": item} for item in df.Party.unique()],
            value=df.Party.unique(),
            multi=True
        ),
        dcc.Graph(
            id='party-attendance-in-time-graph',
            figure=party_attendance([], df)
        ),
        html.H3(children='10. nejhorších poslanců jednotlivých stran podle docházky'),
        dcc.Dropdown(
            id='party-dropdown-worst',
            options=[{"label": item, "value": item} for item in df.Party.unique()],
            value=df.Party.unique(),
            multi=True
        ),
        dcc.Graph(
            id='mps-attendance-worst-graph',
            figure=worst_attendance_mps([], df_0, df_m)
        ),
        html.Br(),
        html.H2(children='Hlasování', id="hlasovani"),
        html.Br(),
        html.H4(children='Počet hlasování ve kterých strany hlasovaly nejednotně'),
        dcc.Graph(
            id='unity',
            figure=unity_party(df_a, df_n)
        ),
        html.H4(children='10. největších rebelů jednotlivých stran'),
        dcc.Dropdown(
            id='party-dropdown-biggest',
            options=[{"label": item, "value": item} for item in df.Party.unique()],
            value=df.Party.unique(),
            multi=True
        ),
        dcc.Graph(
            id='biggest-rebels-graph',
            figure=rebel_mps([], df, df_a, df_n)
        ),
        html.H4(children='Hlasování ve kterých daní poslanci hlasovali proti své straně'),
        dcc.Dropdown(
            id='mps-rebel-dropdown',
            options=[{"label": item, "value": item} for item in df.MP.unique()],
            value=random.choice(df.MP.unique())
        ),
        html.Br(),
        dash_table.DataTable(
            id='table',
            columns=[{"name": i, "id": i} for i in ["Vote", "Vote Title", "Vote description"]],
            data=rebel_mps_prepare_table([], df, df_a, df_n),
        ),
        html.Br(),
        html.H2(children='Hlasovácí koalice', id="hlasovaci-koalice"),
        html.Br(),
        html.H4(children='Korelace ve hlasování jednotlivých stran'),
        html.Span(children='Zvolte alespoň 3 strany'),
        dcc.Dropdown(
            id='correlation-party-dropdown',
            options=[{"label": item, "value": item} for item in df.Party.unique()],
            value=df.Party.unique(),
            multi=True
        ),
        dcc.Graph(
            id='correlation-n-graph',
            figure=correlation_n([], df_n)
        ),
        dcc.Graph(
            id='correlation-a-graph',
            figure=correlation_a([], df_a)
        ),
        html.H4(children='Korelace ve hlasování jednotlivých poslanců'),
        html.Span(children='Zvolte alespoň 3 poslance'),
        dcc.Dropdown(
            id='correlation-mps-dropdown',
            options=[{"label": item, "value": item} for item in df.MP.unique()],
            value=random.choices(df.MP.unique(), k=3),
            multi=True
        ),
        dcc.Graph(
            id='correlation-mps-n-graph',
            figure=correlation_mp_n([], df_n)
        ),
        dcc.Graph(
            id='correlation-mps-a-graph',
            figure=correlation_mp_a([], df_a)
        ),
        html.Br(),
        html.H2(children='Ostatní', id="ostatni"),
        html.Br(),
        html.H4(children='Vývoj počtu poslanců v jednotlivých stranách'),
        dcc.Graph(
            id='count-mps-a-graph',
            figure=number_of_mps_per_party_in_time(df)
        ),
    ])
])


@app.callback(
    dash.dependencies.Output('mps-attendance-graph', 'figure'),
    [dash.dependencies.Input('mps-dropdown', 'value')])
def update_output(value):
    return mps_attendance(value, df)


@app.callback(
    dash.dependencies.Output('party-attendance-in-time-graph', 'figure'),
    [dash.dependencies.Input('party-dropdown', 'value')])
def update_output(value):
    return party_attendance(value, df)


@app.callback(
    dash.dependencies.Output('mps-attendance-worst-graph', 'figure'),
    [dash.dependencies.Input('party-dropdown-worst', 'value')])
def update_output(value):
    return worst_attendance_mps(value, df_0, df_m)


@app.callback(
    dash.dependencies.Output('biggest-rebels-graph', 'figure'),
    [dash.dependencies.Input('party-dropdown-biggest', 'value')])
def update_output(value):
    return rebel_mps(value, df, df_a, df_n)


@app.callback(
    dash.dependencies.Output('table', 'data'),
    [dash.dependencies.Input('mps-rebel-dropdown', 'value')])
def update_output(value):
    return rebel_mps_prepare_table(value, df, df_a, df_n)


@app.callback(
    dash.dependencies.Output('correlation-n-graph', 'figure'),
    [dash.dependencies.Input('correlation-party-dropdown', 'value')])
def update_output(value):
    return correlation_n(value, df_n)


@app.callback(
    dash.dependencies.Output('correlation-a-graph', 'figure'),
    [dash.dependencies.Input('correlation-party-dropdown', 'value')])
def update_output(value):
    return correlation_a(value, df_a)


@app.callback(
    dash.dependencies.Output('correlation-mps-a-graph', 'figure'),
    [dash.dependencies.Input('correlation-mps-dropdown', 'value')])
def update_output(value):
    return correlation_mp_a(value, df_a)


@app.callback(
    dash.dependencies.Output('correlation-mps-n-graph', 'figure'),
    [dash.dependencies.Input('correlation-mps-dropdown', 'value')])
def update_output(value):
    return correlation_mp_n(value, df_n)


if __name__ == '__main__':
    app.run_server(debug=True)
