from functions import mps_attendance, party_attendance, worst_attendance_mps, rebel_mps_prepare_table, correlation_n, \
    correlation_mp_a
import pandas as pd

df = pd.read_csv("export.csv")

df.MP = df.MP + " (" + df.Party + ")"

# create partial data frames
df_a = df[df.Vote == "A"]
df_n = df[df.Vote == "N"]
df_z = df[df.Vote == "Z"]
df_0 = df[df.Vote == "0"]
df_m = df[df.Vote == "M"]


def test_empty_list_behaviour_party_attendance():
    assert party_attendance([], df) == {}


def test_non_sense_input_behaviour_party_attendance():
    assert party_attendance(("ok", "no"), df) == {}


def test_non_sense_df_input_behaviour_party_attendance():
    assert party_attendance(df.Party.unique()[:3], ("ok", "no")) == {}


def test_ok_input_behaviour_party_attendance():
    assert party_attendance(df.Party.unique()[:3], df) != {}


def test_empty_list_behaviour_mps_attendance():
    assert mps_attendance([], df) == {}


def test_non_sense_input_behaviour_mps_attendance():
    assert mps_attendance(("ok", "no"), df) == {}


def test_non_sense_df_input_behaviour_mps_attendance():
    assert mps_attendance(df.MP.unique()[:3], ("ok", "no")) == {}


def test_ok_input_behaviour_mps_attendance():
    assert mps_attendance(df.MP.unique()[:3], df) != {}


def test_empty_list_behaviour_worst_attendance_mps():
    assert worst_attendance_mps([], df_0, df_m) == {}


def test_non_sense_input_behaviour_worst_attendance_mps():
    assert worst_attendance_mps(("ok", "no"), df_0, df_m) == {}


def test_non_sense_df_input_behaviour_worst_attendance_mps():
    assert worst_attendance_mps(df.Party.unique()[:3], ("ok", "no"), df_m) == {}
    assert worst_attendance_mps(df.Party.unique()[:3], df_m, ("ok", "no")) == {}
    assert worst_attendance_mps(df.Party.unique()[:3], ("ok", "no"), ("ok", "no")) == {}


def test_empty_list_behaviour_rebel_mps_prepare_table():
    assert rebel_mps_prepare_table([], df, df_a, df_n) == []


def test_non_sense_input_behaviour_rebel_mps_prepare_table():
    assert rebel_mps_prepare_table(("ok", "no"), df, df_a, df_n) == []


def test_non_sense_df_input_behaviour_rebel_mps_prepare_table():
    assert rebel_mps_prepare_table(df.MP.unique()[0], df, ("ok", "no"), df_n) == []
    assert rebel_mps_prepare_table(df.MP.unique()[0], df, df_a, ("ok", "no")) == []
    assert rebel_mps_prepare_table(df.MP.unique()[0], df, ("ok", "no"), ("ok", "no")) == []
    assert rebel_mps_prepare_table(df.MP.unique()[0], ("ok", "no"), ("ok", "no"), df_n) == []
    assert rebel_mps_prepare_table(df.MP.unique()[0], ("ok", "no"), df_a, ("ok", "no")) == []
    assert rebel_mps_prepare_table(df.MP.unique()[0], ("ok", "no"), ("ok", "no"), ("ok", "no")) == []
    assert rebel_mps_prepare_table(df.MP.unique()[0], ("ok", "no"), df_a, df_n) == []


def test_ok_input_behaviour_rebel_mps_prepare_table():
    assert rebel_mps_prepare_table(df.MP.unique()[0], df, df_a, df_n) != []


def test_empty_list_behaviour_correlation_n():
    assert correlation_n([], df_n) == {}


def test_non_sense_input_behaviour_correlation_n():
    assert correlation_n(("ok", "no"), df_n) == {}


def test_non_sense_df_input_behaviour_correlation_n():
    assert correlation_n(df.Party.unique()[1:4], ("ok", "no")) == {}


def test_ok_input_behaviour_correlation_n():
    assert correlation_n(df.Party.unique()[1:4], df_n) != {}


def test_empty_list_behaviour_correlation_mp_a():
    assert correlation_mp_a([], df_a) == {}


def test_non_sense_input_behaviour_correlation_mp_a():
    assert correlation_mp_a(("ok", "no"), df_a) == {}


def test_non_sense_df_input_behaviour_correlation_mp_a():
    assert correlation_mp_a(df.MP.unique()[1:4], ("ok", "no")) == {}


def test_ok_input_behaviour_correlation_mp_a():
    assert correlation_mp_a(df.MP.unique()[1:4], df_n) != {}
