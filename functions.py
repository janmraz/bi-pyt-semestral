import array

import numpy as np
import pandas as pd
import plotly
from plotly import graph_objs as go, express as px


# each mps attendance in time
def mps_attendance(chosen_mps, df):
    # check correct type
    if not isinstance(chosen_mps, (np.ndarray, list)) or not isinstance(df, pd.core.frame.DataFrame):
        return {}
    # check if there is enough items in list
    if len(chosen_mps) == 0:
        return {}
    mps = df[df.MP.isin(chosen_mps)].drop(columns=["Party", "Vote description"])
    mps.Vote = mps.Vote.apply(lambda x: 0 if x == "O" or x == "M" else 1)
    mps = pd.pivot_table(mps, values='Vote', index=['Vote Id'], columns='MP', aggfunc=np.sum)

    mps_fig = go.Figure()
    for col in mps.columns:
        mps_fig.add_trace(go.Scatter(x=mps.index, y=mps[col].rolling(100).mean() * 100,
                                     name=col,
                                     mode='markers+lines',
                                     line=dict(shape='linear'),
                                     connectgaps=True
                                     )
                          )

    mps_fig.update_xaxes(title_text='Jednotlivá hlasování')
    mps_fig.update_yaxes(title_text='Procentuální účast poslanců')
    mps_fig.update_layout(hovermode="x unified")
    return mps_fig


# party attendance in time
def party_attendance(chosen_parties, df):
    # check correct type
    if not isinstance(chosen_parties, (np.ndarray, list)) or not isinstance(df, pd.core.frame.DataFrame):
        return {}
    # check if there is enough items in list
    if len(chosen_parties) == 0:
        return {}
    # prepare data
    attendance_in_time_data = df[df.Party.isin(chosen_parties)].drop(columns=["Vote Title", "Vote description"])
    attendance_in_time_data['MP'] = 1
    attendance_in_time_data['Vote'] = attendance_in_time_data.Vote.apply(lambda x: 0 if x == "O" or x == "M" else 1)

    # make the computation
    attendance_in_time = attendance_in_time_data.groupby(["Vote Id", "Party"]).sum()
    attendance_in_time['result'] = attendance_in_time.Vote / attendance_in_time.MP * 100
    attendance_in_time = attendance_in_time.drop(columns=["Vote", "MP"]).unstack('Party').reset_index().set_index(
        'Vote Id')
    attendance_in_time.columns = attendance_in_time.columns.droplevel(0)

    # visualisation
    parties = attendance_in_time.rolling(500).mean()

    parties_fig = go.Figure()
    for col in parties.columns:
        value = parties[col]
        parties_fig.add_trace(go.Scatter(x=parties.index, y=value,
                                         name=col,
                                         mode='markers+lines',
                                         line=dict(shape='linear'),
                                         connectgaps=True
                                         )
                              )
    parties_fig.update_xaxes(title_text='Jednotlivá hlasování')
    parties_fig.update_yaxes(title_text='Procentuální účast poslanců jednotlivých stran')
    parties_fig.update_layout(hovermode="x unified")
    return parties_fig


# 10 worst mps by attendance per choosen parties
def worst_attendance_mps(chosen_parties, df_0, df_m):
    # check correct type
    if not isinstance(chosen_parties, (np.ndarray, list)) or not isinstance(df_0,
                                                                            pd.core.frame.DataFrame) or not isinstance(
            df_m, pd.core.frame.DataFrame):
        return {}
    # check if there is enough items in list
    if len(chosen_parties) == 0:
        return {}
    # prepare data
    attendance_mps_data = pd.concat([df_0, df_m])[["MP", "Vote", "Party"]]
    attendance_mps_data = attendance_mps_data[attendance_mps_data.Party.isin(chosen_parties)]
    attendance_mps_data = attendance_mps_data.drop(columns=["Party"])

    # get the computed data
    attendance_mps = attendance_mps_data.groupby("MP").count().sort_values("Vote", ascending=False).head(10)

    # make the visualisation
    figure = attendance_mps.plot(kind='bar', title="10. nejhorších poslanců podle docházky")

    figure.layout.update(showlegend=False)
    figure.update_xaxes(title_text='Poslanci')
    figure.update_yaxes(title_text='Počet hlasování kterých se poslanec neúčastnil')
    return figure


# prepare data for herd vote (shared)
def prepare_herd_vote(df_a, df_n):
    # check correct type
    if not isinstance(df_a, pd.core.frame.DataFrame) or not isinstance(df_n, pd.core.frame.DataFrame):
        return {}
    # prepare data
    herd_vote_data = pd.concat([df_a, df_n])[["MP", "Vote", "Party", "Vote Id"]]
    herd_vote_data["Vote_A"] = herd_vote_data.Vote.apply(lambda x: 1 if x == "A" else 0)
    herd_vote_data["Vote_N"] = herd_vote_data.Vote.apply(lambda x: 1 if x == "N" else 0)

    # get get sum of votes per party per vote
    herd_vote = herd_vote_data.groupby(["Vote Id", "Party"]).sum().reset_index()

    return herd_vote


# prepare data for herd mentality for each mps
def prepare_herd_data(df_a, df_n):
    # check correct type
    if not isinstance(df_a, pd.core.frame.DataFrame) or not isinstance(df_n, pd.core.frame.DataFrame):
        return {}
    herd_vote = prepare_herd_vote(df_a, df_n)
    # create helper column - is column Vote_A a majority ?
    herd_vote['result'] = herd_vote["Vote_A"] > herd_vote["Vote_N"]

    # divide data into two parts - A is majority and N is majority
    herd_vote_n = herd_vote[~herd_vote['result']]
    herd_vote_a = herd_vote[herd_vote['result']]

    # get all votes where parties had at least one vote opposite
    herd_vote_n_res = herd_vote_n[herd_vote_n["Vote_A"] > 0].drop(columns=['Vote_A', 'Vote_N', 'result'])
    herd_vote_a_res = herd_vote_a[herd_vote_a["Vote_N"] > 0].drop(columns=['Vote_A', 'Vote_N', 'result'])

    # add another helper column to mark the opposite of majority vote
    herd_vote_n_res['Vote'] = "A"
    herd_vote_a_res['Vote'] = "N"

    # merge them together
    herd_vote_res = pd.concat([herd_vote_n_res, herd_vote_a_res])

    return herd_vote_res


def rebel_mps_prepare_data(df, df_a, df_n):
    # check correct type
    if not isinstance(df, pd.core.frame.DataFrame) or not isinstance(df_a, pd.core.frame.DataFrame) or not isinstance(
            df_n, pd.core.frame.DataFrame):
        return {}
    herd_vote_res = prepare_herd_data(df_a, df_n)
    # stackoverflow magic :D
    # it takes data and transform them into tuples and then filter with them original values
    # goal is to get mps that voted incorrectly (= against the party majority vote)
    tuples = [tuple(x) for x in herd_vote_res.to_numpy()]
    result = df[pd.Series(list(zip(df['Vote Id'], df['Party'], df['Vote']))).isin(tuples)]

    return result


def rebel_mps_prepare_table(chosen_mp, df, df_a, df_n):
    # check correct type
    if not isinstance(df, pd.core.frame.DataFrame) or not isinstance(df_a, pd.core.frame.DataFrame) or not isinstance(
            df_n, pd.core.frame.DataFrame):
        return []
    rebel_mps_data = rebel_mps_prepare_data(df, df_a, df_n)
    chosen_mps = [chosen_mp]
    return rebel_mps_data[rebel_mps_data.MP.isin(chosen_mps)].to_dict('records')


def rebel_mps(chosen_parties, df, df_a, df_n):
    # check correct type
    if not isinstance(df, pd.core.frame.DataFrame) or not isinstance(df_a, pd.core.frame.DataFrame) or not isinstance(
            df_n, pd.core.frame.DataFrame):
        return {}
    # check if there is enough items in list
    if len(chosen_parties) == 0:
        return {}
    rebel_mps_data = rebel_mps_prepare_data(df, df_a, df_n)

    # get count of rebel actions for every MP and then choose the 10 worst
    top_rebel_mps = rebel_mps_data[rebel_mps_data.Party.isin(chosen_parties)].MP.value_counts().head(10)
    figure = top_rebel_mps.plot(kind='bar',
                                title="10. největších rebelů (rebel=poslanec co hlasuje jinak než většina jeho strany)")

    figure.update_layout(showlegend=False)
    figure.update_xaxes(title_text='Poslanci')
    figure.update_yaxes(title_text='Počet hlasování ve kterých poslanec hlasoval jinak než jeho strana')

    return figure


def unity_party(df_a, df_n):
    # check correct type
    if not isinstance(df_a, pd.core.frame.DataFrame) or not isinstance(df_n, pd.core.frame.DataFrame):
        return {}
    herd_vote = prepare_herd_vote(df_a, df_n)
    # create helper column - is column Vote_A a majority ?
    herd_vote['result'] = herd_vote["Vote_A"] > herd_vote["Vote_N"]

    # divide data into two parts - A is majority and N is majority
    herd_vote_n = herd_vote[~herd_vote['result']]
    herd_vote_a = herd_vote[herd_vote['result']]

    # get data where parties voted as one
    herd_vote_n_res_party = herd_vote_n[herd_vote_n["Vote_A"] != 0].drop(columns=['Vote_A', 'Vote_N', 'result'])
    herd_vote_a_res_party = herd_vote_a[herd_vote_a["Vote_N"] != 0].drop(columns=['Vote_A', 'Vote_N', 'result'])

    # add another helper column to mark the opposite of majority vote
    herd_vote_n_res_party['Vote'] = "A"
    herd_vote_a_res_party['Vote'] = "N"

    # merge them together
    herd_vote_res = pd.concat([herd_vote_n_res_party, herd_vote_a_res_party]).drop(columns=['Vote Id'])

    # group the count per party and plot the result
    figure = herd_vote_res.groupby("Party").count().sort_values("Vote", ascending=False).plot(
        kind='bar', title='Počet hlasování kdy strany hlasovaly nejednotně (alespoň jeden poslanec hlasoval opačně)')

    figure.update_layout(showlegend=False)
    figure.update_xaxes(title_text='Strany')
    figure.update_yaxes(title_text='Počet hlasování ve kterých strana hlasovala nejednotně')

    return figure


def correlation_n(chosen_parties, df_n):
    # check correct type
    if not isinstance(df_n, pd.core.frame.DataFrame):
        return {}
    # check if there is enough items in list
    if len(chosen_parties) < 3:
        return {}
    # prepare data
    corr_df_data_n = df_n[df_n.Party.isin(chosen_parties)].drop(columns=['Vote description', 'Vote Title', 'MP'])

    # compute the results
    corr_matrix_n = corr_df_data_n.groupby(["Vote Id", "Party"]).count().unstack('Party').fillna(0).corr()

    fig_n = px.imshow(corr_matrix_n, labels=dict(x="", y="", color="Míra podobnosti při hlasování NE"),
                      x=chosen_parties,
                      y=chosen_parties,
                      color_continuous_scale='Bluered_r')

    return fig_n


def correlation_a(chosen_parties, df_a):
    # check correct type
    if not isinstance(df_a, pd.core.frame.DataFrame):
        return {}
    # check if there is enough items in list
    if len(chosen_parties) < 3:
        return {}
    # prepare data
    corr_df_data_a = df_a[df_a.Party.isin(chosen_parties)].drop(columns=['Vote description', 'Vote Title', 'MP'])

    # compute the results
    corr_matrix_a = corr_df_data_a.groupby(["Vote Id", "Party"]).count().unstack('Party').fillna(0).corr()

    fig_a = px.imshow(corr_matrix_a, labels=dict(x="", y="", color="Míra podobnosti při hlasování ANO"),
                      x=chosen_parties,
                      y=chosen_parties,
                      color_continuous_scale='Bluered_r')

    return fig_a


def correlation_mp_n(chosen_mps, df_n):
    # check correct type
    if not isinstance(df_n, pd.core.frame.DataFrame):
        return {}
    # check if there is enough items in list
    if len(chosen_mps) < 3:
        return {}
    # prepare data
    corr_df_data_n = df_n[df_n.MP.isin(chosen_mps)].drop(columns=['Vote description', 'Vote Title', 'Party'])

    # compute the results
    corr_matrix_n = corr_df_data_n.groupby(["Vote Id", "MP"]).count().unstack('MP').fillna(0).corr()

    fig_n = px.imshow(corr_matrix_n, labels=dict(x="", y="", color="Míra podobnosti při hlasování NE"),
                      x=chosen_mps,
                      y=chosen_mps,
                      color_continuous_scale='Bluered_r')

    return fig_n


def correlation_mp_a(chosen_mps, df_a):
    # check correct type
    if not isinstance(df_a, pd.core.frame.DataFrame):
        return {}
    # check if there is enough items in list
    if len(chosen_mps) < 3:
        return {}
    # prepare data
    corr_df_data_a = df_a[df_a.MP.isin(chosen_mps)].drop(columns=['Vote description', 'Vote Title', 'Party'])

    # compute the results
    corr_matrix_a = corr_df_data_a.groupby(["Vote Id", "MP"]).count().unstack('MP').fillna(0).corr()

    fig_a = px.imshow(corr_matrix_a, labels=dict(x="", y="", color="Míra podobnosti při hlasování ANO"),
                      x=chosen_mps,
                      y=chosen_mps,
                      color_continuous_scale='Bluered_r')

    return fig_a


# attendance of each party in percent
def full_percentual_attendance_party(df):
    # check correct type
    if not isinstance(df, pd.core.frame.DataFrame):
        return {}
    attendance_data = df.drop(columns=["Vote description", "Vote Id"])
    attendance_data['MP'] = 1
    attendance_data['Vote'] = attendance_data.Vote.apply(lambda x: 0 if x == "O" or x == "M" else 1)

    attendance = attendance_data.groupby("Party").sum()
    attendance['result'] = attendance.Vote / attendance.MP * 100
    attendance = attendance.drop(columns=['Vote', "MP"]).sort_values("result", ascending=False)

    attendance_fig = attendance.plot(kind='bar')
    attendance_fig.update_layout(showlegend=False)
    attendance_fig.update_xaxes(title_text='Jednotlivé strany')
    attendance_fig.update_yaxes(title_text='Procentuální účast poslanců jednotlivých stran')

    return attendance_fig


# Show time-based number of MPs per Party
def number_of_mps_per_party_in_time(df):
    # check correct type
    if not isinstance(df, pd.core.frame.DataFrame):
        return {}

    time_based_number_mps_data = df[df.Party != "ANO"].drop(columns=["Vote Title", "Vote", "Vote description"])

    # computation
    time_based_number_mps = time_based_number_mps_data.groupby(["Vote Id", "Party"]).count().unstack(
        'Party').reset_index().set_index('Vote Id')
    time_based_number_mps.columns = time_based_number_mps.columns.droplevel(0)

    nr_mps_party_fig = time_based_number_mps.plot()
    nr_mps_party_fig.update_xaxes(title_text='Jednotlivá hlasování')
    nr_mps_party_fig.update_yaxes(title_text='Počet poslanců za danou stranu')

    return nr_mps_party_fig
